FROM gradle:jdk17

# GET AUDIVERIS CODE
# RUN apk update && apk add git
RUN apt-get update && apt-get install -y git
WORKDIR /
RUN git clone https://github.com/Audiveris/audiveris.git

# BUILD
WORKDIR /audiveris
RUN git checkout 5.2.5
# RUN apk update && apk add tesseract-ocr tesseract-ocr-data-ita
RUN apt-get update && apt-get install -y tesseract-ocr tesseract-ocr-ita
RUN gradle wrapper --stacktrace
RUN ./gradlew clean build
RUN tar -xf build/distributions/Audiveris-5.3-alpha.tar

# CHECK WORKING
ADD test_partiture.png /audiveris/data
RUN Audiveris-5.3-alpha/bin/Audiveris -batch -export -output data/ data/test_partiture.png
RUN cd data/test_partiture && unzip test_partiture.omr

WORKDIR /app
# PYTHON
# ENV PYTHONUNBUFFERED=1
# RUN apk add --update --no-cache python3 python3-dev py3-pip gfortran build-base && ln -sf python3 /usr/bin/python
RUN apt-get update && apt-get install -y python3 python3-pip
# RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

# APP
# RUN apk add --update --no-cache py3-numpy py3-scipy
RUN apt-get update && apt-get install -y lilypond libsndfile-dev
ADD requirements.txt .
RUN pip3 install -r requirements.txt

ADD app.py .
ADD test_partiture.png .
CMD streamlit run app.py --server.port=8501 --server.address=0.0.0.0