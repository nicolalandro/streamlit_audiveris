import os
from PIL import Image
import streamlit as st
import music21

st.title('test')

image_path = 'test_partiture.png'
image = Image.open(image_path)
st.image(image, caption='Uploaded imaged')

if st.button('transcribe'):
    os.system("rm -rf test")
    os.system(f'/audiveris/Audiveris-5.3-alpha/bin/Audiveris -batch -export -option org.audiveris.omr.sheet.BookManager.useOpus=true -option org.audiveris.omr.sheet.BookManager.useCompression=false -output test/ {image_path}')
    os.system('cd test/test_partiture && unzip test_partiture.opus.mxl')

    file_path = "test/test_partiture/test_partiture.opus.xml"
    music = music21.converter.parse(file_path, format='musicxml')
    streaming_partiture = str(music.write('lily.png'))
    image = Image.open(streaming_partiture)
    st.text('Partiture')
    st.image(image)